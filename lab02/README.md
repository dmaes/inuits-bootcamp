# VM Web Server and Proxy

## Vagrant
Create a Vagrantfile (based on lab01's Vagrantfile) with 3 VM's.
All 3 VM's must be able to communicate with each other via some network.

## Provisioning
### SSL
Create a self-signed certificate to use with nginx:
```sh
# openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/ssl/private/lab02-selfsigned.key -out /etc/ssl/certs/lab02-selfsigned.crt -subj "/C=BE/ST=Antwerp/L=Antwerp/O=Inuits/OU=Inuits/CN=lab02"
```

### NGINX
Install NGINX on all 3 machines via dnf:
```sh
# dnf install -y nginx
```

A systemd service file is provided by the nginx rpm,
but the service must still be started and enabled:
```sh
# systemctl start nginx.service
# systemctl enable nginx.service
```

Create `index.html` on **lab02B** and **lab02C** containing their respective hostnames:
```sh
# cat /etc/hostname > /usr/share/nginx/html/index.html
```

To configure load-balancing on **lab02A**, define the following 'server' in /etc/nginx/conf.d/lab02.conf
```
upstream lab02_workers {
	server lab02B;
	server lab02C;
}

server {
	listen 80;
	server_name lab02;

	location / {
		proxy_pass http://lab02_workers;
	}
}

server {
	listen 443;
	server_name lab02;

	ssl on;
	ssl_certificate /etc/ssl/certs/lab02-selfsigned.crt;
	ssl_certificate_key /etc/ssl/private/lab02-selfsigned.key;

	location / {
		proxy_pass http://lab02_workers;
	}
}
```

Also on **lab02A**, set SELinux policy to permissive, or nginx will not be permitted to proxy:
```sh
# setenforce permissive
```

## Resources
* https://www.vagrantup.com/docs/multi-machine
* https://www.vagrantup.com/docs/networking/private_network
* http://nginx.org/en/docs/http/load_balancing.html
