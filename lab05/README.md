# Puppet

## Vagrant
Setup 4 nodes (i.e. the 3 nodes from lab03 and 1 extra as Puppet-master)

## Puppet master
Install the puppetserver:
```sh
# rpm -U https://yum.puppet.com/puppet6-release-el-8.noarch.rpm
" dnf install -y puppetserver
" systemctl start puppetserver && systemctl enable puppetserver
```

By default puppetserver uses 2GB of RAM. To change this, edit `/etc/sysconfig/puppetserver` (e.g. 512MB):
```sh
JAVA_ARGS="-Xms512m -Xmx512m"
```
