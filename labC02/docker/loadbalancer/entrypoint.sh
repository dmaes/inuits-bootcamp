#! /bin/sh

rm -f /etc/nginx/workers.conf

for worker in "$@"; do
	echo "server $worker;" >> /etc/nginx/workers.conf
done

cat /etc/nginx/workers.conf

nginx -t

exec nginx
