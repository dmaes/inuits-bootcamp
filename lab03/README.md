# VM Web Server and Proxy

## Base setup
Use a similar setup as in lab02

## Python app
Ensure the app's files are present under `/usr/local/bin/app`

If you need a Python version that is not available in the CentOS repositories, compile it:
```sh
# dnf group install -y "Development Tools"
# dnf install -y openssl-devel bzip2-devel libffi-devel
$ curl https://www.python.org/ftp/python/3.8.5/Python-3.8.5.tgz > /tmp/python-3.8.5.tgz
$ cd /tmp && tar -xzf python-3.8.5.tgz && cd /tmp/Python-3.8.5
$ ./configure --enable-optimizations
# make altinstall
```

Install the python app (system install):
```sh
$ cd /usr/local/bin/app
# /usr/local/bin/pipenv --python /usr/local/bin/python3.8 install --system --deploy
```

If you want, you can now manually run the Python app:
```sh
$ cd /usr/local/bin/app
$ /usr/local/bin/pipenv --python /usr/local/bin/python3.8 run /usr/local/bin/gunicorn --bind 0.0.0.0:5000 --access-logfile - main:app
```

However, a `systemd`-service is recommended, so create one in `/lib/systemd/system/app.service`:
```systemd
[Unit]
Description = Python app
After = network.target network-online.target
Wants = network-online.target

[Service]
WorkingDirectory = /usr/local/bin/app
ExecStart = /usr/local/bin/pipenv --python /usr/local/bin/python3.8 run /usr/local/bin/gunicorn --bind 0.0.0.0:5000 --access-logfile - main:app

[Install]
WantedBy = multi-user.target
```

## Load balancer
For the loadbalancer, reuse from lab02, adapt names and server ports in the `upstreams` section.

## Resources
* https://computingforgeeks.com/how-to-install-python-on-3-on-centos/
