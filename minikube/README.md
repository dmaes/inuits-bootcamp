# Kubernetes with Minikube

## Setup

1. Setup base CentOS 8 box
2. Install docker (and optionally docker-compose), add your user to `docker` group !
3. Install minikube
```sh
dnf install -y https://storage.googleapis.com/minikube/releases/latest/minikube-latest.x86_64.rpm
```
4. Install kubectl
```sh
curl -L https://storage.googleapis.com/kubernetes-release/release/v1.18.6/bin/linux/amd64/kubectl -o /usr/local/bin/kubectl
chmod +x /usr/local/bin/kubectl
```
5. Start minikube cluster
```sh
minikube start
```
6. You can deploy on your cluster with `kubectl`

## Deploy loadbalanced app
1. Get app.yml on CentOS 8 box
2. Apply app.yml with `kubectl`
```sh
kubectl apply -f app.yml
```

## Test
To test if you can access:
1. port-forward `service/loadbalancer` for easy access on `localhost`:
```sh
kubectl port-forward service/loadbalancer 7080:80
```
2. Test with cURL
```sh
curl http://localhost:7080
```
