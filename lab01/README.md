# VM CentOS Base

*Done on Fedora 32, adapt to your own system*

## Setup Virtualization Environment

We will use VirtualBox as a hypervisor and Vagrant to easily manage VM's

Ensure UEFI SecureBoot is disabled!

Install VirtualBox:
```sh
# wget http://download.virtualbox.org/virtualbox/rpm/fedora/virtualbox.repo -P /etc/yum.repos.d/
# dnf update
# dnf install @development-tools
# dnf install kernel-devel kernel-headers dkms qt5-qtx11extras  elfutils-libelf-devel zlib-devel
# dnf install VirtualBox
```

Install Vagrant:
```sh
$ wget https://releases.hashicorp.com/vagrant/2.2.9/vagrant_2.2.9_linux_amd64.zip
$ unzip vagrant_2.2.9_linux_amd64.zip
# mv vagrant /usr/local/bin/vagrant
```

## Install CentOS Vagrant box
Create new centos/8 Vagrant box:
```sh
$ vagrant init centos/8 \
	--box-version 1905.1
$ vagrant up
```

If the vagrant box is succesfully created and booted, you can access it with
```sh
$ vagrant ssh
```

To stop the vagrant box:
```sh
$ vagrant halt
```

To destroy the vagrant box:
```sh
$ vagrant destroy
```

## Configure CentOS
### Create `admin` user
First create the admin user:
```sh
# useradd admin
```

Set `admin` password:
```sh
# echo admin:admin | chpasswd
```

Give `admin` user sudo rights by adding to the `wheel` group:
```sh
# usermod admin -aG wheel
```

### Install Python3
Install python3 (includes pip)
```sh
# dnf install python3
```

## References
* https://www.tecmint.com/install-virtualbox-in-fedora-linux/
